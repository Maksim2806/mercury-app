/**
 * @template T
 * @param {(element:T) => boolean} predicate 
 * @param {T[]} collection 
 * @returns {T[]}
 */

 const filter = (predicate, collection) => {
    return collection.reduce((acc, item) => {
        const filteredItem = predicate(item) ? [item] : []
        return [...acc, ...filteredItem]
    }, [])
}