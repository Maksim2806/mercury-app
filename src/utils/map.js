/**
 * @template T
 * @template V
 * @param {(element: T) => V} callback 
 * @param {T[]} collection 
 * @returns {V[]}
 */

 const map = (callback, collection) => {
    return collection.reduce((acc, item) => [...acc, callback(item)], [])
 }